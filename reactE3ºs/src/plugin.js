'use strict'

import React, { Component } from 'react'

class Plugin extends Component {
  constructor () {
    super()
    this.handleClick = this.handleClick.bind(this)
  }

  handleClick () {
    console.log('clicou no botão', this.myInput)
    this.myInput.focus()
  }

  // ref permite vc mesmo manipular o DOM (N recomendado)
  // ref só funciona em componentes smart(statefull)
  render () {
    return (
      <div>
        <input type='text' ref={(node) => (this.myInput = node)} />
        <button onClick={this.handleClick}>Focus</button>
      </div>
    )
  }
}

export default Plugin
