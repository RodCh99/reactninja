'use strict'

console.log('________ TESTES ________ \n')

const sum = require('./sum')

//  verificando se sum é uma função:
console.assert(
  typeof sum === 'function',
  '\nsum deve ser uma função!'
)

//  verificando se a função está retornando 3 para a entrada 1, 2:
console.assert(
  sum(1, 2) === 3,
  '\nsoma de 1 e 2 deveria retornar 3'
)

// verificar se a função está retornando 5 para a entrada 2, 3:
console.assert(
  sum(2, 3) === 5,
  '\nsoma de 2 e 3 deveria retornar 5'
)


console.log('\n___  FIM DOS TESTES  ___')
