'use strict'

import React from 'react'
// import Counter from './counter'
import { connect } from 'react-redux'

// componente stateless com redux:
const Counter = ({ counter, handleDecrement, handleIncrement }) => (
  <div>
    <h1>{counter}</h1>
    <button onClick={handleDecrement}>-</button>
    <button onClick={handleIncrement}>+</button>
  </div>
)

const mapStateToProps = (state) => ({
  counter: state
})

const mapDispatchToProps = (dispatch) => ({
  handleIncrement: () => dispatch({ type: 'INCREMENT' }),
  handleDecrement: () => dispatch({ type: 'DECREMENT' })
})

export default connect(mapStateToProps, mapDispatchToProps)(Counter)
