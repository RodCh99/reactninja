'use strict'
import React, { Component } from 'react'

class Timer extends Component {
  constructor () {
    super()
    this.state = {
      time: 0
    }
    this.timer
  }

  // momento em que o componente recebe as novas props:
  // Só é executado quando o componente recebe novas props
  componentWillReceiveProps (nextProps) {
    console.log('ComponentWillReciveProps Timer', this.props, nextProps)
  }

  // toda vez que o componente é renderizado o esse método
  // é chamado antes
  shouldComponentUpdate (nextProps, nextState) {
    // console.log('shouldComponentUpdate Timer', this.props, nextProps)
    // se for false ele não atualiza o componente
    return this.props.time !== nextProps.time
  }

  // umpouco antes do componente ser renderizado novamente
  // assim que ele for atualizado
  // não se deve usar o this.setState()
  componentWillUpdate (nextProps, nextState) {
    console.log('componentWillUpdate Timer', this.props, nextProps)
  }

  // componente chamado após a atualização do componente:
  componentDidUpdate (prevProps, prevState) {
    console.log('componentDidUpdate', this.props, prevProps)
  }

  // Executa somente uma vez
  componentDidMount () {
    console.log('componentDidMount Timer')
    this.timer = setInterval(() => {
      this.setState({
        time: this.state.time + 1
      })
    }, 1000)
  }

  componentWillUnmount () {
    clearInterval(this.timer)
  }

  render () {
    console.log('render Timer')
    return (
      <div>Timer: {this.state.time}</div>
    )
  }
}

export default Timer
