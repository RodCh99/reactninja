
'use strict'

import React, { Component } from 'react'
import Timer from './timer'

//  usando classes do ES2015
class App extends Component {
  constructor () {
    console.log('Construtor app')
    super()
    this.state = {
      time: 0,
      showTimer: true
    }
  }

  // Mount significa que o componente está pronto para uso, está no DOM:
  componentWillMount () {
    console.log('ComponentWillMount app')
  }

  // Manipulação de DOM deve ser feita aqui!
  componentDidMount () {
    console.log('componentDidMount app')
  }

  render () {
    console.log('render app')
    return (
      <div>
        <Timer time={this.state.time} />
        <button onClick={() => {
          this.setState({ time: this.state.time + 10 })
        }}
        >Change Props
        </button>
      </div>
    )
  }
}

export default App
