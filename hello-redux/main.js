'use strict'

// 1º Reducer, componente que faz as alterações na store:
const counter = (state = 0, action) => {

switch (action.type) {
    case 'INCREMENT': return state + 1
    case 'DECREMENT': return state - 1
  }

  return state
}



/*
  CreateStore:

    | é o objeto principal  todos os estados da aplicação ficarão
    | armazenados dentro da store!  possui métodos para disparar
    | ações e ouvir sobre ações.

  store:

   possui 3 métodos principais:

   | 1º] getState():
   |
   |   Método retorna o estado atual que foi salvo na store.
   |
   | 2º] dispatch({ type: '#$%' }):
   |
   |   Método que dispara uma ação.
   |
   | 3º] subscribe([Func]):
   |
   |   Executa a função que foi passada com parametro
   |   sempre que uma ação for executada.

*/


// Importando o redux do index.html:
const { createStore } =  Redux

// Instanciando a store:
const store = createStore(counter)

// Instanciando variaveis vindas do html:
const $counter = document.querySelector('[data-js="counter"]')
const $decrement = document.querySelector('[data-js="decrement"]')
const $increment = document.querySelector('[data-js="increment"]')

// Adicionando eventos para as variaveis de invremento e decrescimento:
$decrement.addEventListener('click', decrement, false)
$increment.addEventListener('click', increment, false)

// Funções que serão realizadas nos eventos:
function decrement () { 

  // Função realizando um método da store para ativar uma action no Reducer
  store.dispatch({ type: 'DECREMENT' })
}

// Funções que serão realizadas nos eventos:
function increment () { 

  // Função realizando um método da store para ativar uma action no Reducer
  store.dispatch({ type: 'INCREMENT' })
}

// Método que fica ouvindo a store e realizando uma função sempre que  
// o dispatch for executado.
store.subscribe(() => {

  $counter.textContent = store.getState()

})

store.subscribe(() => {
  console.log('State: ', store.getState())
})
