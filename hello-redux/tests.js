//               TDD:

// primeiro teste:
console.assert(
  counter(0, { type: 'INCREMENT' }) === 1
)


// segundo teste:
console.assert(
  counter(1, { type: 'INCREMENT' }) === 2
)

// Teste de outra ação:
console.assert(
  counter(2, { type: 'DECREMENT' }) == 1
)

console.assert(
 counter(5, { type: 'DECREMENT' }) === 4
)


// Teste de ações que não existem:
console.assert(
  counter(3, { type: 'SOMETHING' }) === 3
)

// Primeira vez que o estado estiver na aplicação:
console.assert(
  counter(undefined, {}) === 0
)
